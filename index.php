<?php
    session_start();

    include "config/config.php";

    if (isset($_SESSION['user_id']) && $_SESSION!==null) {
       header("location: dashboard.php");
    }

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Iniciar Sesión </title>

        <!-- Bootstrap -->
        <link href="css/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="css/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- NProgress -->
        <link href="css/nprogress/nprogress.css" rel="stylesheet">
        <!-- Animate.css -->
        <link href="css/animate.css/animate.min.css" rel="stylesheet">

        <!-- Custom Theme Style -->
        <link href="css/custom.min.css" rel="stylesheet">

    </head>
    <body class="login">
        <div>
            <a class="hiddenanchor" id="signup"></a>
            <a class="hiddenanchor" id="signin"></a>
            <div class="login_wrapper">
                <div class="animate form login_form">
                    <?php 
                        $invalid=sha1(md5("contrasena y email invalido"));
                        if (isset($_GET['invalid']) && $_GET['invalid']==$invalid) {
                            echo "<div class='alert alert-danger alert-dismissible fade in' role='alert'>
                                <strong>Error!</strong> Contraseña o correo Electrónico invalido
                                </div>";
                        }
                    ?>
                    <section class="login_content">
                        <form action="action/login.php" method="post" style="border-style: solid;box-shadow: 4px 4px lightblue;
                       padding:30px;background:cornflowerblue;color: floralwhite;border-color:transparent;border-radius: 20px;">
                            <h1 style="background:coral;margin-left:-33px;height: 40px;width: 123%;">
                               <div style="margin-top: 10px!important;position: absolute;text-align: center;width:100%">Iniciar Sesión</div></h1>
                            <div>
                                <input type="text" name="email" class="form-control" placeholder="Correo Electrónico" required />
                            </div>
                            <div>
                                <input type="password" name="password" class="form-control" placeholder="Contraseña" required/>
                            </div>
                            
                                <button type="submit" name="token" value="Login" class="btn btn-success">Iniciar Sesion</button>
                            <div>   
                            <br>
                             <a class="reset_pass" style="color:gainsboro;text-decoration: underline;font-style: italic;" href="#">Olvidaste Tu contraseña?</a>
                            </div>
                            <div class="clearfix"></div>
                            <div class="separator">
                                <div class="clearfix"></div>
                                <br />
                                <div>
                                    <h1><i class="fa fa-ticket"></i> SIMAS</h1>
                                    <p> <a style="color:white;" target="_blank" href="#">Sistema integrado de mesa de ayuda y servicios 1.0</a></p>
                                </div>
                            </div>
                           <hr>
                        </form>
                    </section>
                </div>
            </div>
        </div>
    </body>
</html>
