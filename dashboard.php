<?php 
    $title ="Dashboard - "; 
    include "head.php";
    include "sidebar.php";

    if($_SESSION['nivel']=='1'){
        $TicketData=mysqli_query($con, "select * from ticket where status_id in (1,2)");
    }else if($_SESSION['nivel']=='2'){
        $TicketData=mysqli_query($con, "select * from ticket where status_id in (1,2) and asigned_id= ". $_SESSION['user_id']);
    }else{
        $TicketData=mysqli_query($con, "select * from ticket ");
    }  


    if($_SESSION['nivel']=='1' || $_SESSION['nivel']=='2'){
        $ProjectData=mysqli_query($con, "select * from project where idempresa = " . $_SESSION['idEmpresa']);
    }else{
        $ProjectData=mysqli_query($con, "select * from project ");
    }
   // $ProjectData=mysqli_query($con, "select * from project where idempresa = " . $_SESSION['idEmpresa']);
    $CategoryData=mysqli_query($con, "select * from category");
    $UserData=mysqli_query($con, "select * from user order by created_at desc");
?>
    <div class="right_col" role="main"> <!-- page content -->
        <div class="">
            <div class="page-title">
                <div class="row top_tiles">
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats" style="background:cornflowerblue;color: black;border-style: outset;">
                          <div class="icon"><i style="color: black;" class="fa fa-ticket"></i></div>
                          <div class="count"><?php echo mysqli_num_rows($TicketData) ?></div>
                          <h3 style="color: white;">Mis Tickets</h3>
                        </div>
                    </div>
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats" style="background:cornflowerblue;color: black;border-style: outset;">
                          <div class="icon"><i style="color: black;" class="fa fa-list-alt"></i></div>
                          <div class="count"><?php echo mysqli_num_rows($ProjectData) ?></div>
                          <h3 style="color: white;">Proyectos</h3>
                        </div>
                    </div>
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats" style="background:cornflowerblue;color: black;border-style: outset;">
                          <div class="icon"><i style="color: black;" class="fa fa-th-list"></i></div>
                          <div class="count"><?php echo mysqli_num_rows($CategoryData) ?></div>
                          <h3 style="color: white;">Categorias</h3>
                        </div>
                    </div>
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats" style="background:cornflowerblue;color: black;border-style: outset;">
                          <div class="icon"><i style="color: black;" class="fa fa-users"></i></div>
                          <div class="count"><?php echo mysqli_num_rows($UserData) ?></div>
                          <h3 style="color: white;">Usuarios</h3>
                        </div>
                    </div>
                </div>
                <!-- content -->
                
                <div class="row">
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <?php include "lib/alerts.php";
                            profile(); //llamada a la funcion de alertas
                        ?>    
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Dashboard</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                </ul>
                            <div class="clearfix"></div>
                            </div>
                            <div class="x_content" style="text-align: center;">
                                <br />
                                <img style="width:95%;" src="images/dashboard.png" alt="" >
                            </div>
                        </div>
                    </div>
<!--
                    <div class="col-md-6 col-xs-12 col-sm-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Indicadores</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                </ul>
                            <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <br />
                                <br /> <br /> <br /> <br /> <br />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-12 col-sm-12">                           
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Indicadores</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                </ul>
                            <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <br />
                                <br /> <br /> <br /> <br /> <br />
                            </div>
                        </div>
                 </div>
-->
                </div>
            </div>
        </div>
    </div><!-- /page content -->

<?php include "footer.php" ?>
<script>
    $(function(){
        $("input[name='file']").on("change", function(){
            var formData = new FormData($("#formulario")[0]);
            var ruta = "action/upload-profile.php";
            $.ajax({
                url: ruta,
                type: "POST",
                data: formData,
                contentType: false,
                processData: false,
                success: function(datos)
                {
                    $("#respuesta").html(datos);
                }
            });
        });
    });
</script>