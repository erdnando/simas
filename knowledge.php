<?php
    $title ="Base de conocimientos | ";
    include "head.php";
    include "sidebar.php";
?>
        
    <div class="right_col" role="main"><!-- page content -->
        <div class="">
            <div class="page-title">
                <div class="clearfix"></div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                  
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Base de conocimientos </h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        
                        <!-- form search -->
                        <form class="form-horizontal" role="form" id="category_expence">
                            <div class="form-group row">
                            <h1 style="text-align:center">¿Como podemos ayudarte?</h1>
                            <br>
                                <label for="q" class="col-md-2 control-label"></label>
                                <div class="col-md-4">
                                
                                    <input type="text" class="form-control" id="q" placeholder="Empieza escribiendo una pregunta ..." onkeyup='load(1);'>
                                    <div>Temas populares: <strong>Cuentas de buró | Correo lleno</strong></div>
                                </div>
                                <div class="col-md-3">
                                    <button type="button" class="btn btn-default" onclick='load(1);'>
                                        <span class="glyphicon glyphicon-search" ></span> Buscar</button>
                                    <span id="loader"></span>
                                </div>
                            </div>
                        </form>    
                        <!-- end form search -->

                    </div>
                </div>




                <div class="col-md-12 col-sm-12 col-xs-12">
                   <br>
                    <div class=" row">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="x_panel" style="height:200px;">
                                <div class="row" style="text-align: center;" >
                                  <div class="icon"><i style="color: coral;font-size:xxx-large" class="fa fa-road"></i></div>
                                </div>
                                <br>
                                <div class="row" style="text-align: center;">Guia de inicio</div>
                                <div class="row" style="text-align: center;">Esta guia corta cubre lo basico para crear y enviar sus estimaciones</div>
                                <div class="row" style="text-align: center;">
                                    <span  class="btn btn-my-button btn-success" style="width:50%!important;background:cornflowerblue">
                                        Vamos
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="x_panel" style="height:200px;">
                                <div class="row" style="text-align: center;" >
                                    <div class="icon"><i style="color: coral;font-size:xxx-large" class="fa fa-question-circle"></i></div>
                                </div>
                                <br>
                                <div class="row" style="text-align: center;">¿Necesita ayuda?</div>
                                <div class="row" style="text-align: center;">Estamos aqui para ayudarte en cada paso delproceso. Aprenda todo acerca de nuestros recursos de soporte</div>
                                <div class="row" style="text-align: center;">
                                    <span class="btn btn-my-button btn-success" style="width:50%!important;background:cornflowerblue">
                                        Obtenga ayuda
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="x_panel" style="height:200px;">
                                <div class="row" style="text-align: center;" >
                                    <div class="icon"><i style="color: coral;font-size:xxx-large" class="fa fa-graduation-cap"></i></div>
                                </div>
                                <br>
                                <div class="row" style="text-align: center;">Conectar</div>
                                <div class="row" style="text-align: center;">Mantengase informado con nuestros avisos, noticias, comunicados y mas en el blog institucional</div>
                                <div class="row" style="text-align: center;">
                                    <span class="btn btn-my-button btn-success" style="width:50%!important;background:cornflowerblue">
                                        Aprenda mas
                                    </span>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <hr/> 
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                   <br>
                    <div class=" row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel" style="height:130px;">
                               <table style="width:100%;margin-top:20px">
                                  <tr>
                                    <td>
                                       <ul>
                                        <li><a style="text-decoration: underline;" href='#'>¿Como obtener la ultima versión de la plantilla maestra?</a></li>
                                        <li><a style="text-decoration: underline;" href='#'>¿Como registrar una oportunidad?</a></li>
                                        <li><a style="text-decoration: underline;" href='#'>¿Como actualizar el estatus de su oportunidad?</a></li>
                                       </ul>
                                    </td>
                                    <td>
                                       
                                    </td>
                                    <td>
                                      <ul>
                                        <li><a style="text-decoration: underline;" href='#'>¿Como recuperar documentos eliminados?</a></li>
                                        <li><a style="text-decoration: underline;" href='#'>¿Donde estan las fichas tecnicas?</a></li>
                                        <li><a style="text-decoration: underline;" href='#'>¿Como activo mi cuenta?</a></li>
                                       </ul>
                                    </td>
                                  </tr>
                                  
                               </table>
                            </div>
                        </div> 
                    </div>
                    <hr/> 
                </div>

                



            </div>
        </div>
    </div><!-- /page content -->


<?php include "footer.php" ?>

<script type="text/javascript" src="js/category.js"></script>

<script>
$( "#add" ).submit(function( event ) {
  $('#save_data').attr("disabled", true);
  
 var parametros = $(this).serialize();
     $.ajax({
            type: "POST",
            url: "action/addcategory.php",
            data: parametros,
             beforeSend: function(objeto){
                $("#result").html("Mensaje: Cargando...");
              },
            success: function(datos){
            $("#result").html(datos);
            $('#save_data').attr("disabled", false);
            load(1);
          }
    });
  event.preventDefault();
})

// success

$( "#upd" ).submit(function( event ) {
  $('#upd_data').attr("disabled", true);
  
 var parametros = $(this).serialize();
     $.ajax({
            type: "POST",
            url: "action/updcategory.php",
            data: parametros,
             beforeSend: function(objeto){
                $("#result2").html("Mensaje: Cargando...");
              },
            success: function(datos){
            $("#result2").html(datos);
            $('#upd_data').attr("disabled", false);
            load(1);
          }
    });
  event.preventDefault();
})

    function obtener_datos(id){
            var name = $("#name"+id).val();
            $("#mod_id").val(id);
            $("#mod_name").val(name);
        }
</script>