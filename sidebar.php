        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu"><!-- sidebar menu -->
            <div class="menu_section">
                <ul class="nav side-menu">
                   <!-- <li class="<?php if(isset($active1)){echo $active1;}?>">
                        <a href="dashboard.php"><i class="fa fa-dashboard"></i> <span style="color:floralwhite!important">Dashboard</span></a>
                    </li>

                    <li class="<?php if(isset($active2)){echo $active2;}?>">
                        <a href="tickets.php"><i class="fa fa-ticket"></i> <span style="color:floralwhite">Tickets</span></a>
                    </li>

                    <li class="<?php if(isset($active3)){echo $active3;}?>">
                        <a href="empresas.php"><i class="fa fa-industry"></i> <span style="color:floralwhite">Empresas</span></a>
                    </li>

                    <li class="<?php if(isset($active4)){echo $active4;}?>">
                        <a href="projects.php"><i class="fa fa-list-alt"></i> <span style="color:floralwhite">Proyectos</span></a>
                    </li>

                    <li class="<?php if(isset($active5)){echo $active5;}?>">
                        <a href="categories.php"><i class="fa fa-align-left"></i> <span style="color:floralwhite">Categorias</span></a>
                    </li>

                    <li class="<?php if(isset($active6)){echo $active6;}?>">
                        <a href="reports.php"><i class="fa fa-area-chart"></i> <span style="color:floralwhite">Descubre</span></a>
                    </li>

                    <li class="<?php if(isset($active7)){echo $active7;}?>">
                        <a href="users.php"><i class="fa fa-users"></i> <span style="color:floralwhite">Usuarios</span></a>
                    </li>

                    <li class="<?php if(isset($active8)){echo $active8;}?>">
                        <a href="knowledge.php"><i class="fa fa-question-circle"></i> <span style="color:floralwhite">Base de conocimientos</span></a>
                    </li>-->

                    <?php if($_SESSION['nivel']=='1' || $_SESSION['nivel']=='3' || $_SESSION['nivel']=='2'){  ?>
                        <li class="<?php if(isset($active1)){echo $active1;}?>">
                            <a href="dashboard.php"><i class="fa fa-dashboard"></i> <span style="color:floralwhite!important">Dashboard</span></a>
                        </li>
                     <?php }?>

                     <?php if($_SESSION['nivel']=='1' || $_SESSION['nivel']=='3' || $_SESSION['nivel']=='2'){  ?>
                        <li class="<?php if(isset($active2)){echo $active2;}?>">
                            <a href="tickets.php"><i class="fa fa-ticket"></i> <span style="color:floralwhite">Tickets</span></a>
                        </li>
                    <?php }?>

                    <?php if($_SESSION['nivel']=='1' || $_SESSION['nivel']=='3'){  ?>
                        <li class="<?php if(isset($active3)){echo $active3;}?>">
                            <a href="empresas.php"><i class="fa fa-industry"></i> <span style="color:floralwhite">Empresas</span></a>
                        </li>
                    <?php }?>

                    <?php if($_SESSION['nivel']=='1' || $_SESSION['nivel']=='3'){  ?>
                        <li class="<?php if(isset($active4)){echo $active4;}?>">
                            <a href="projects.php"><i class="fa fa-list-alt"></i> <span style="color:floralwhite">Proyectos</span></a>
                        </li>
                    <?php }?>

                    <?php if($_SESSION['nivel']=='3' || $_SESSION['nivel']=='3'){  ?>
                        <li class="<?php if(isset($active5)){echo $active5;}?>">
                            <a href="categories.php"><i class="fa fa-align-left"></i> <span style="color:floralwhite">Categorias</span></a>
                        </li>
                    <?php }?>

                    <?php if($_SESSION['nivel']=='1' || $_SESSION['nivel']=='3' ){  ?>
                        <li class="<?php if(isset($active6)){echo $active6;}?>">
                            <a href="reports.php"><i class="fa fa-area-chart"></i> <span style="color:floralwhite">Descubre</span></a>
                        </li>
                    <?php }?>

                    <?php if($_SESSION['nivel']=='3' || $_SESSION['nivel']=='3'){  ?>
                        <li class="<?php if(isset($active7)){echo $active7;}?>">
                            <a href="users.php"><i class="fa fa-users"></i> <span style="color:floralwhite">Usuarios</span></a>
                        </li>
                    <?php }?>

                    <?php if($_SESSION['nivel']=='1' || $_SESSION['nivel']=='3' || $_SESSION['nivel']=='2'){  ?>
                        <li class="<?php if(isset($active8)){echo $active8;}?>">
                            <a href="knowledge.php"><i class="fa fa-question-circle"></i> <span style="color:floralwhite">Base de conocimientos</span></a>
                        </li>
                    <?php }?>

                    <?php if($_SESSION['nivel']=='1' || $_SESSION['nivel']=='3' || $_SESSION['nivel']=='2'){  ?>
                        <li class="<?php if(isset($active9)){echo $active9;}?>">
                            <a href="perfil.php"><i class="fa fa-user"></i> <span style="color:floralwhite">Perfil</span></a>
                        </li>
                    <?php }?>

                </ul>
            </div>
        </div><!-- /sidebar menu -->
    </div>
</div> 
     
    <div class="top_nav"><!-- top navigation -->
        <div class="nav_menu" style="background:black">
            <nav>
                <div class="nav toggle">
                    <a id="menu_toggle"><i style="color: white;" class="fa fa-bars"></i></a>
                </div>
                <ul class="nav navbar-nav navbar-right">
                    <li class="">
                        <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <img src="images/profiles/<?php echo $profile_pic;?>" alt="" >
                            <span style="color:cornflowerblue"><?php echo $name;?></span>
                            <span class=" fa fa-angle-down"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-usermenu pull-right" style="">
                            <li><a href="dashboard.php"><i class="fa fa-user"></i> Mi cuenta</a></li>
                            <li><a href="action/logout.php"><i class="fa fa-sign-out pull-right"></i> Cerrar Sesión</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </div>
    </div><!-- /top navigation -->    