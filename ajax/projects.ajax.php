<?php
require_once "../config/conexion.php";//Contiene funcion que conecta a la base de datos
//require_once "../config/config.php";//Contiene funcion que conecta a la base de datos

//require_once "../controladores/usuarios.controlador.php";
//require_once "../modelos/usuarios.modelo.php";

class AjaxProyectos{


	/*=============================================
	EMAIL REFERENCIADO
	=============================================*/
	public $idEmpresa;

	public function ajaxGetProjectsByEnterprise(){

		$con = Conexion::conn();
		$empresa = $this->idEmpresa;
				
		$projectos = mysqli_query($con, "select * from project where idEmpresa=" . $empresa);
		
		$arrProjects=array();

		while ($row=mysqli_fetch_array($projectos)) {

			$item = (object) [
				'value' => $row['id'],
				'text' => $row['name'],
			  ];
			  array_push($arrProjects,$item);
           // array_push($arrProjects,$row['id'],$row['name']);	  
		}

        echo json_encode($arrProjects);
	}

}
/*=============================================
VALIDAR EMAIL EXISTENTE
=============================================*/	
if(isset($_POST["idEmpresa"])){

	$usuario = new AjaxProyectos();
	$usuario -> idEmpresa = $_POST["idEmpresa"];

	$usuario ->ajaxGetProjectsByEnterprise();
}
