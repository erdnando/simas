$(document).ready(function(){
	load(1);
});

function load(page){
	var q= $("#q").val();
	$("#loader").fadeIn('slow');
	$.ajax({
		url:'./ajax/tickets.php?action=ajax&page='+page+'&q='+q,
		beforeSend: function(objeto){
			$('#loader').html('<img src="./images/ajax-loader.gif"> Cargando...');
		},
		success:function(data){
			$(".outer_div").html(data).fadeIn('slow');
			$('#loader').html('');
		}
	});
}

function empresaSeleccionada(item){
  console.log(item.value);
  
    var datos = new FormData();
    datos.append("idEmpresa", item.value);

    $.ajax({
        url: "ajax/projects.ajax.php",
        method: "POST",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        success: function(respuesta) {
			//TODO: reload cbo projects
			console.log(JSON.parse(respuesta));
			var objArrProjectos = JSON.parse(respuesta);

			$('#project_id').children().remove().end();

			$('#project_id').append($('<option>').text("-- Selecciona --").attr('value', 0));
			$.each(objArrProjectos, function(i, item) {
				$('#project_id').append($('<option>').text(item.text).attr('value', item.value));
			  });
               
        }
    });


}

function eliminar (id)
{
	var q= $("#q").val();
	if (confirm("Realmente deseas eliminar el ticket?")){	
		$.ajax({
			type: "GET",
			url: "./ajax/tickets.php",
			data: "id="+id,"q":q,
			beforeSend: function(objeto){
				$("#resultados").html("Mensaje: Cargando...");
			},
			success: function(datos){
				$("#resultados").html(datos);
				load(1);
			}
		});
	}
}