<?php	
	session_start();
	/*Inicia validacion del lado del servidor*/
	if (empty($_POST['title'])) {
           $errors[] = "Titulo vacío";
        } else if (empty($_POST['description'])){
			$errors[] = "Description vacío";
		}  else if ($_POST['empresa_id'] == '0'){
			$errors[] = "Seleccione una empresa";
		} else if ($_POST['project_id'] == '0'){
			$errors[] = "Seleccione un proyecto";
		} else if (empty($_POST['category_id'])){
			$errors[] = "Seleccione una categoria";
		} else if (empty($_POST['priority_id'])){
			$errors[] = "Seleccione una prioridad";
		} else if (empty($_POST['status_id'])){
			$errors[] = "Selecciones un estatus";
		} else if (empty($_POST['asignado_id'])){
			$errors[] = "Asigne el ticket";
		}else if (
			!empty($_POST['title']) &&
			!empty($_POST['description'])
		) {


		include "../config/config.php";//Contiene funcion que conecta a la base de datos

		$title = $_POST["title"];//ok
		$description = $_POST["description"];//ok
		$empresa_id = $_POST["empresa_id"];
		$project_id = $_POST["project_id"];
		$category_id = $_POST["category_id"];
		$priority_id = $_POST["priority_id"];
		$status_id = $_POST["status_id"];
		$asignado_id = $_POST["asignado_id"];

		$user_id = $_SESSION["user_id"];
		$kind_id = $_POST["kind_id"];
		$created_at="NOW()";

		// $user_id=$_SESSION['user_id'];

		$sql="insert into ticket (title,description,category_id,project_id,priority_id,user_id,status_id,kind_id,created_at,idEmpresa,asigned_id) value (\"$title\",\"$description\",\"$category_id\",\"$project_id\",$priority_id,$user_id,$status_id,$kind_id,$created_at,$empresa_id,$asignado_id)";

		$query_new_insert = mysqli_query($con,$sql);
			if ($query_new_insert){
				$messages[] = "Tu ticket ha sido ingresado satisfactoriamente.";
			} else{
				$errors []= "Lo siento algo ha salido mal intenta nuevamente.".mysqli_error($con);
			}
		} else {
			$errors []= "Error desconocido.";
		}
		
		if (isset($errors)){
			
			?>
			<div class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Error!</strong> 
					<?php
						foreach ($errors as $error) {
								echo $error;
							}
						?>
			</div>
			<?php
			}
			if (isset($messages)){
				
				?>
				<div class="alert alert-success" role="alert">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<strong>¡Bien hecho!</strong>
						<?php
							foreach ($messages as $message) {
									echo $message;
								}
							?>
				</div>
				<?php
			}

?>